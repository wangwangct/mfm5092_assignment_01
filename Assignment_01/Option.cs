﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_01
{
    //This class calculate all the outputs we want
    public class Option
    {
        //instan. simulator calss
        Simulator sim = new Simulator();

       
        public void scheduler()
        {
            //instan. a random matrix of the size
            double[,] r = new double[IO.Trials, IO.Steps];
            double[,] pricing_matrix = new double[1, IO.Trials+1];
            //compute and generate this random matrix
            r = sim.random();
           //As we need to calculate SE, option price of each simulation is needed, so we assign values to a matrix
            pricing_matrix = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);

            //calculate price, Greek values and SE
            IO.O_price = pricing_matrix[0, 0];
            IO.O_se = SE(pricing_matrix);
            IO.O_delta = Delta(r);
            IO.O_gamma = Gamma(r);
            IO.O_vega = Vega(r);
            IO.O_theta = Theta(r);
            IO.O_rho = Rho(r);
 
            
            //Call Garbage Collector to release RAM
            GC.Collect();
        }
        //compute price, the output of this method is a 1X(1+IO.Trials) matrix. The 1st column is option price, the rest are prices of each simulation
        public double[,] Price(double[,] r,int Trials, int Steps,double Tenor,double S,double K, double R, double vol )
        {
            if (IO.Call == true)//call option
            {
                double[,] price = new double[Trials, Steps + 1];
                double dt = Tenor / Convert.ToDouble(Steps);

                for (int a = 0; a <= Trials-1; a++)
                {
                    price[a, 0] = S;
                }
                for (int a = 0; a <= Trials-1; a++)
                {
                    for (int b = 1; b <= Steps; b++)
                    {
                        price[a, b] = price[a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) + (vol * Math.Sqrt(dt) * r[a, b - 1]));
                    }
                }
                double sum = 0;
                double[,] Result = new double[1, Trials + 1];
                for (int a = 0; a < Trials; a++)
                {
                    sum += Math.Max(price[a, Steps] - K, 0);
                    
                }

                Result[0,0]= sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                for (int a = 1; a <= Trials; a++) 
                {
                    Result[0, a] = Math.Max(price[a-1, Steps] - K, 0) * Math.Exp(-(R * Tenor));
                }
                
                return Result;
            }
            else//put option
            {
                double[,] price = new double[Trials, Steps + 1];
                double dt = Tenor / Convert.ToDouble(Steps);

                for (int a = 0; a <= Trials - 1; a++)
                {
                    price[a, 0] = S;
                }
                for (int a = 0; a <= Trials - 1; a++)
                {
                    for (int b = 1; b <= Steps; b++)
                    {
                        price[a, b] = price[a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) + (vol * Math.Sqrt(dt) * r[a, b - 1]));
                    }
                }
                double sum = 0;
                double[,] Result = new double[1, Trials + 1];
                for (int a = 0; a < Trials; a++)
                {
                    sum += Math.Max(K - price[a, Steps], 0);

                }

                Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                for (int a = 1; a <= Trials; a++)
                {
                    Result[0, a] = Math.Max(K - price[a-1, Steps], 0) * Math.Exp(-(R * Tenor));
                }

                return Result;
            }

        }

        //The following part calculates Greek values and SE. 
        //The pricing_matrix is used to save the output of Price method, and we just need the 1st lattice of each matrix to calculate Greek values.
        public double Delta(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, IO.Trials + 1];
            double[,] pricing_matrix_2 = new double[1, IO.Trials + 1];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying + (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying - (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = (pricing_matrix_1[0, 0] - pricing_matrix_2[0, 0]) / (0.002 * IO.Underlying);
            return result;
        }

        public double Gamma(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, IO.Trials + 1];
            double[,] pricing_matrix_2 = new double[1, IO.Trials + 1];
            double[,] pricing_matrix_3 = new double[1, IO.Trials + 1];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying +(0.001*IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying , IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_3 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying -(0.001*IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = (pricing_matrix_1[0, 0] - (2 * pricing_matrix_2[0, 0]) + pricing_matrix_3[0, 0]) / Math.Pow(0.001 * IO.Underlying, 2);
            return result;
        }

        public double Vega(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, IO.Trials + 1];
            double[,] pricing_matrix_2 = new double[1, IO.Trials + 1];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility+(0.001*IO.Volatility));
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility-(0.001*IO.Volatility));
            double result = (pricing_matrix_1[0, 0] - pricing_matrix_2[0, 0]) / (0.002 * IO.Volatility);
            return result;
        }

        public double Theta(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, IO.Trials + 1];
            double[,] pricing_matrix_2 = new double[1, IO.Trials + 1];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor+(IO.Tenor/Convert.ToDouble(IO.Steps)), IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = -(pricing_matrix_1[0, 0] - pricing_matrix_2[0, 0]) / (IO.Tenor / Convert.ToDouble(IO.Steps));
            return result;
        }

        public double Rho(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, IO.Trials + 1];
            double[,] pricing_matrix_2 = new double[1, IO.Trials + 1];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate+(0.001*IO.Risk_free_rate), IO.Volatility);
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate-(0.001*IO.Risk_free_rate), IO.Volatility);
            double result = (pricing_matrix_1[0, 0] - pricing_matrix_2[0, 0]) / (0.002 * IO.Risk_free_rate);   
            return result;
        }

        //Calculate standard error, the input of this method is already calculated when calculating option price.
        public double SE(double[,] pricing_matrix)
        {
            double sum = 0;
            for (int a = 1; a <= IO.Trials; a++) 
            {
                sum += Math.Pow(pricing_matrix[0, a] - pricing_matrix[0, 0], 2);
            }
            double SD = Math.Sqrt((sum) / (IO.Trials-1));
            double result = SD / Math.Sqrt(IO.Trials);
            return result;
        }
    }
}
