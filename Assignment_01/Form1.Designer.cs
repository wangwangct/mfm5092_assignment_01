﻿namespace Assignment_01
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lable_trials = new System.Windows.Forms.Label();
            this.lable_steps = new System.Windows.Forms.Label();
            this.label_tenor = new System.Windows.Forms.Label();
            this.label_S = new System.Windows.Forms.Label();
            this.label_K = new System.Windows.Forms.Label();
            this.label_r = new System.Windows.Forms.Label();
            this.label_vol = new System.Windows.Forms.Label();
            this.textBox_trials = new System.Windows.Forms.TextBox();
            this.textBox_steps = new System.Windows.Forms.TextBox();
            this.textBox_tenor = new System.Windows.Forms.TextBox();
            this.textBox_S = new System.Windows.Forms.TextBox();
            this.textBox_K = new System.Windows.Forms.TextBox();
            this.textBox_r = new System.Windows.Forms.TextBox();
            this.textBox_vol = new System.Windows.Forms.TextBox();
            this.radioButton_call = new System.Windows.Forms.RadioButton();
            this.radioButton_put = new System.Windows.Forms.RadioButton();
            this.button_go = new System.Windows.Forms.Button();
            this.textBox_price = new System.Windows.Forms.TextBox();
            this.label_price = new System.Windows.Forms.Label();
            this.textBox_delta = new System.Windows.Forms.TextBox();
            this.label_delta = new System.Windows.Forms.Label();
            this.textBox_gamma = new System.Windows.Forms.TextBox();
            this.textBox_vega = new System.Windows.Forms.TextBox();
            this.textBox_theta = new System.Windows.Forms.TextBox();
            this.textBox_rho = new System.Windows.Forms.TextBox();
            this.textBox_se = new System.Windows.Forms.TextBox();
            this.label_gamma = new System.Windows.Forms.Label();
            this.label_vega = new System.Windows.Forms.Label();
            this.label_theta = new System.Windows.Forms.Label();
            this.label_rho = new System.Windows.Forms.Label();
            this.label_se = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lable_trials
            // 
            this.lable_trials.AutoSize = true;
            this.lable_trials.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable_trials.Location = new System.Drawing.Point(13, 9);
            this.lable_trials.Name = "lable_trials";
            this.lable_trials.Size = new System.Drawing.Size(44, 18);
            this.lable_trials.TabIndex = 0;
            this.lable_trials.Text = "Trials";
            // 
            // lable_steps
            // 
            this.lable_steps.AutoSize = true;
            this.lable_steps.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable_steps.Location = new System.Drawing.Point(13, 39);
            this.lable_steps.Name = "lable_steps";
            this.lable_steps.Size = new System.Drawing.Size(46, 18);
            this.lable_steps.TabIndex = 1;
            this.lable_steps.Text = "Steps";
            // 
            // label_tenor
            // 
            this.label_tenor.AutoSize = true;
            this.label_tenor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_tenor.Location = new System.Drawing.Point(13, 69);
            this.label_tenor.Name = "label_tenor";
            this.label_tenor.Size = new System.Drawing.Size(47, 18);
            this.label_tenor.TabIndex = 2;
            this.label_tenor.Text = "Tenor";
            // 
            // label_S
            // 
            this.label_S.AutoSize = true;
            this.label_S.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_S.Location = new System.Drawing.Point(13, 99);
            this.label_S.Name = "label_S";
            this.label_S.Size = new System.Drawing.Size(115, 18);
            this.label_S.TabIndex = 3;
            this.label_S.Text = "Underlying Price";
            // 
            // label_K
            // 
            this.label_K.AutoSize = true;
            this.label_K.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_K.Location = new System.Drawing.Point(13, 129);
            this.label_K.Name = "label_K";
            this.label_K.Size = new System.Drawing.Size(84, 18);
            this.label_K.TabIndex = 4;
            this.label_K.Text = "Strike Price";
            // 
            // label_r
            // 
            this.label_r.AutoSize = true;
            this.label_r.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_r.Location = new System.Drawing.Point(13, 159);
            this.label_r.Name = "label_r";
            this.label_r.Size = new System.Drawing.Size(110, 18);
            this.label_r.TabIndex = 5;
            this.label_r.Text = "Risk_free  Rate";
            // 
            // label_vol
            // 
            this.label_vol.AutoSize = true;
            this.label_vol.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_vol.Location = new System.Drawing.Point(13, 189);
            this.label_vol.Name = "label_vol";
            this.label_vol.Size = new System.Drawing.Size(61, 18);
            this.label_vol.TabIndex = 6;
            this.label_vol.Text = "Volatility";
            // 
            // textBox_trials
            // 
            this.textBox_trials.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_trials.Location = new System.Drawing.Point(182, 6);
            this.textBox_trials.Name = "textBox_trials";
            this.textBox_trials.Size = new System.Drawing.Size(100, 24);
            this.textBox_trials.TabIndex = 7;
            this.textBox_trials.Leave += new System.EventHandler(this.textBox_trials_Leave);
            // 
            // textBox_steps
            // 
            this.textBox_steps.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_steps.Location = new System.Drawing.Point(182, 36);
            this.textBox_steps.Name = "textBox_steps";
            this.textBox_steps.Size = new System.Drawing.Size(100, 24);
            this.textBox_steps.TabIndex = 8;
            this.textBox_steps.Leave += new System.EventHandler(this.textBox_steps_Leave);
            // 
            // textBox_tenor
            // 
            this.textBox_tenor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_tenor.Location = new System.Drawing.Point(182, 66);
            this.textBox_tenor.Name = "textBox_tenor";
            this.textBox_tenor.Size = new System.Drawing.Size(100, 24);
            this.textBox_tenor.TabIndex = 9;
            this.textBox_tenor.Leave += new System.EventHandler(this.textBox_tenor_Leave);
            // 
            // textBox_S
            // 
            this.textBox_S.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_S.Location = new System.Drawing.Point(182, 96);
            this.textBox_S.Name = "textBox_S";
            this.textBox_S.Size = new System.Drawing.Size(100, 24);
            this.textBox_S.TabIndex = 10;
            this.textBox_S.Leave += new System.EventHandler(this.textBox_S_Leave);
            // 
            // textBox_K
            // 
            this.textBox_K.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_K.Location = new System.Drawing.Point(182, 126);
            this.textBox_K.Name = "textBox_K";
            this.textBox_K.Size = new System.Drawing.Size(100, 24);
            this.textBox_K.TabIndex = 11;
            this.textBox_K.Leave += new System.EventHandler(this.textBox_K_Leave);
            // 
            // textBox_r
            // 
            this.textBox_r.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_r.Location = new System.Drawing.Point(182, 156);
            this.textBox_r.Name = "textBox_r";
            this.textBox_r.Size = new System.Drawing.Size(100, 24);
            this.textBox_r.TabIndex = 12;
            this.textBox_r.Leave += new System.EventHandler(this.textBox_r_Leave);
            // 
            // textBox_vol
            // 
            this.textBox_vol.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_vol.Location = new System.Drawing.Point(182, 186);
            this.textBox_vol.Name = "textBox_vol";
            this.textBox_vol.Size = new System.Drawing.Size(100, 24);
            this.textBox_vol.TabIndex = 13;
            this.textBox_vol.Leave += new System.EventHandler(this.textBox_vol_Leave);
            // 
            // radioButton_call
            // 
            this.radioButton_call.AutoSize = true;
            this.radioButton_call.Checked = true;
            this.radioButton_call.Location = new System.Drawing.Point(38, 241);
            this.radioButton_call.Name = "radioButton_call";
            this.radioButton_call.Size = new System.Drawing.Size(42, 17);
            this.radioButton_call.TabIndex = 14;
            this.radioButton_call.TabStop = true;
            this.radioButton_call.Text = "Call";
            this.radioButton_call.UseVisualStyleBackColor = true;
            // 
            // radioButton_put
            // 
            this.radioButton_put.AutoSize = true;
            this.radioButton_put.Location = new System.Drawing.Point(182, 241);
            this.radioButton_put.Name = "radioButton_put";
            this.radioButton_put.Size = new System.Drawing.Size(41, 17);
            this.radioButton_put.TabIndex = 15;
            this.radioButton_put.TabStop = true;
            this.radioButton_put.Text = "Put";
            this.radioButton_put.UseVisualStyleBackColor = true;
            // 
            // button_go
            // 
            this.button_go.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_go.Location = new System.Drawing.Point(74, 277);
            this.button_go.Name = "button_go";
            this.button_go.Size = new System.Drawing.Size(115, 56);
            this.button_go.TabIndex = 16;
            this.button_go.Text = "Go!";
            this.button_go.UseVisualStyleBackColor = true;
            this.button_go.Click += new System.EventHandler(this.button_go_Click);
            // 
            // textBox_price
            // 
            this.textBox_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_price.Location = new System.Drawing.Point(577, 6);
            this.textBox_price.Name = "textBox_price";
            this.textBox_price.Size = new System.Drawing.Size(100, 24);
            this.textBox_price.TabIndex = 17;
            // 
            // label_price
            // 
            this.label_price.AutoSize = true;
            this.label_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_price.Location = new System.Drawing.Point(464, 9);
            this.label_price.Name = "label_price";
            this.label_price.Size = new System.Drawing.Size(42, 18);
            this.label_price.TabIndex = 18;
            this.label_price.Text = "Price";
            // 
            // textBox_delta
            // 
            this.textBox_delta.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_delta.Location = new System.Drawing.Point(577, 39);
            this.textBox_delta.Name = "textBox_delta";
            this.textBox_delta.Size = new System.Drawing.Size(100, 24);
            this.textBox_delta.TabIndex = 19;
            // 
            // label_delta
            // 
            this.label_delta.AutoSize = true;
            this.label_delta.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_delta.Location = new System.Drawing.Point(464, 42);
            this.label_delta.Name = "label_delta";
            this.label_delta.Size = new System.Drawing.Size(42, 18);
            this.label_delta.TabIndex = 20;
            this.label_delta.Text = "Delta";
            // 
            // textBox_gamma
            // 
            this.textBox_gamma.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_gamma.Location = new System.Drawing.Point(577, 69);
            this.textBox_gamma.Name = "textBox_gamma";
            this.textBox_gamma.Size = new System.Drawing.Size(100, 24);
            this.textBox_gamma.TabIndex = 21;
            // 
            // textBox_vega
            // 
            this.textBox_vega.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_vega.Location = new System.Drawing.Point(577, 99);
            this.textBox_vega.Name = "textBox_vega";
            this.textBox_vega.Size = new System.Drawing.Size(100, 24);
            this.textBox_vega.TabIndex = 22;
            // 
            // textBox_theta
            // 
            this.textBox_theta.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_theta.Location = new System.Drawing.Point(577, 129);
            this.textBox_theta.Name = "textBox_theta";
            this.textBox_theta.Size = new System.Drawing.Size(100, 24);
            this.textBox_theta.TabIndex = 23;
            // 
            // textBox_rho
            // 
            this.textBox_rho.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_rho.Location = new System.Drawing.Point(577, 159);
            this.textBox_rho.Name = "textBox_rho";
            this.textBox_rho.Size = new System.Drawing.Size(100, 24);
            this.textBox_rho.TabIndex = 24;
            // 
            // textBox_se
            // 
            this.textBox_se.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_se.Location = new System.Drawing.Point(577, 189);
            this.textBox_se.Name = "textBox_se";
            this.textBox_se.Size = new System.Drawing.Size(100, 24);
            this.textBox_se.TabIndex = 25;
            // 
            // label_gamma
            // 
            this.label_gamma.AutoSize = true;
            this.label_gamma.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_gamma.Location = new System.Drawing.Point(464, 72);
            this.label_gamma.Name = "label_gamma";
            this.label_gamma.Size = new System.Drawing.Size(62, 18);
            this.label_gamma.TabIndex = 26;
            this.label_gamma.Text = "Gamma";
            // 
            // label_vega
            // 
            this.label_vega.AutoSize = true;
            this.label_vega.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_vega.Location = new System.Drawing.Point(464, 99);
            this.label_vega.Name = "label_vega";
            this.label_vega.Size = new System.Drawing.Size(41, 18);
            this.label_vega.TabIndex = 27;
            this.label_vega.Text = "Vega";
            // 
            // label_theta
            // 
            this.label_theta.AutoSize = true;
            this.label_theta.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_theta.Location = new System.Drawing.Point(464, 132);
            this.label_theta.Name = "label_theta";
            this.label_theta.Size = new System.Drawing.Size(45, 18);
            this.label_theta.TabIndex = 28;
            this.label_theta.Text = "Theta";
            // 
            // label_rho
            // 
            this.label_rho.AutoSize = true;
            this.label_rho.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_rho.Location = new System.Drawing.Point(464, 162);
            this.label_rho.Name = "label_rho";
            this.label_rho.Size = new System.Drawing.Size(36, 18);
            this.label_rho.TabIndex = 29;
            this.label_rho.Text = "Rho";
            // 
            // label_se
            // 
            this.label_se.AutoSize = true;
            this.label_se.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_se.Location = new System.Drawing.Point(464, 189);
            this.label_se.Name = "label_se";
            this.label_se.Size = new System.Drawing.Size(105, 18);
            this.label_se.TabIndex = 30;
            this.label_se.Text = "Standard Error";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 539);
            this.Controls.Add(this.label_se);
            this.Controls.Add(this.label_rho);
            this.Controls.Add(this.label_theta);
            this.Controls.Add(this.label_vega);
            this.Controls.Add(this.label_gamma);
            this.Controls.Add(this.textBox_se);
            this.Controls.Add(this.textBox_rho);
            this.Controls.Add(this.textBox_theta);
            this.Controls.Add(this.textBox_vega);
            this.Controls.Add(this.textBox_gamma);
            this.Controls.Add(this.label_delta);
            this.Controls.Add(this.textBox_delta);
            this.Controls.Add(this.label_price);
            this.Controls.Add(this.textBox_price);
            this.Controls.Add(this.button_go);
            this.Controls.Add(this.radioButton_put);
            this.Controls.Add(this.radioButton_call);
            this.Controls.Add(this.textBox_vol);
            this.Controls.Add(this.textBox_r);
            this.Controls.Add(this.textBox_K);
            this.Controls.Add(this.textBox_S);
            this.Controls.Add(this.textBox_tenor);
            this.Controls.Add(this.textBox_steps);
            this.Controls.Add(this.textBox_trials);
            this.Controls.Add(this.label_vol);
            this.Controls.Add(this.label_r);
            this.Controls.Add(this.label_K);
            this.Controls.Add(this.label_S);
            this.Controls.Add(this.label_tenor);
            this.Controls.Add(this.lable_steps);
            this.Controls.Add(this.lable_trials);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lable_trials;
        private System.Windows.Forms.Label lable_steps;
        private System.Windows.Forms.Label label_tenor;
        private System.Windows.Forms.Label label_S;
        private System.Windows.Forms.Label label_K;
        private System.Windows.Forms.Label label_r;
        private System.Windows.Forms.Label label_vol;
        private System.Windows.Forms.TextBox textBox_trials;
        private System.Windows.Forms.TextBox textBox_steps;
        private System.Windows.Forms.TextBox textBox_tenor;
        private System.Windows.Forms.TextBox textBox_S;
        private System.Windows.Forms.TextBox textBox_K;
        private System.Windows.Forms.TextBox textBox_r;
        private System.Windows.Forms.TextBox textBox_vol;
        private System.Windows.Forms.RadioButton radioButton_call;
        private System.Windows.Forms.RadioButton radioButton_put;
        private System.Windows.Forms.Button button_go;
        private System.Windows.Forms.TextBox textBox_price;
        private System.Windows.Forms.Label label_price;
        private System.Windows.Forms.TextBox textBox_delta;
        private System.Windows.Forms.Label label_delta;
        private System.Windows.Forms.TextBox textBox_gamma;
        private System.Windows.Forms.TextBox textBox_vega;
        private System.Windows.Forms.TextBox textBox_theta;
        private System.Windows.Forms.TextBox textBox_rho;
        private System.Windows.Forms.TextBox textBox_se;
        private System.Windows.Forms.Label label_gamma;
        private System.Windows.Forms.Label label_vega;
        private System.Windows.Forms.Label label_theta;
        private System.Windows.Forms.Label label_rho;
        private System.Windows.Forms.Label label_se;
    }
}

